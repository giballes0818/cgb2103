package cn.tedu.sp01.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {
    private Integer id;
    private String name;      // 名称
    private Integer number;   // 数量

    // public static void main(String[] args) {
    //     new Item().setName("sdsf");
    // }
}
